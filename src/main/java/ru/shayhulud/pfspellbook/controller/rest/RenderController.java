package ru.shayhulud.pfspellbook.controller.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.shayhulud.pfspellbook.domain.model.Spell;
import ru.shayhulud.pfspellbook.domain.model.Spellbook;
import ru.shayhulud.pfspellbook.domain.render.SpellCardRenderDTO;
import ru.shayhulud.pfspellbook.domain.render.SpellbookRenderDTO;
import ru.shayhulud.pfspellbook.exception.NotFoundException;
import ru.shayhulud.pfspellbook.service.SpellCardRenderer;
import ru.shayhulud.pfspellbook.service.SpellService;
import ru.shayhulud.pfspellbook.service.SpellbookRenderer;
import ru.shayhulud.pfspellbook.service.SpellbookService;

/**
 * Controller for spell.
 */
@RestController
@RequiredArgsConstructor
@Api(value = "/api", tags = "render info")
public class RenderController {

	private final SpellCardRenderer spellCardRenderer;
	private final SpellService spellService;

	private final SpellbookRenderer spellbookRenderer;
	private final SpellbookService spellbookService;

	@GetMapping("/api/spell/{id}/render/info")
	@ApiOperation("Get spell render info by id")
	@ApiResponses({
		@ApiResponse(
			code = 200,
			message = "Success"
		),
		@ApiResponse(
			code = 404,
			message = "Spell with this id not found. Possible error key\n" +
				"* " + NotFoundException.ERROR_TEXT,
			response = String.class, responseContainer = "Map"
		)
	})
	public SpellCardRenderDTO renderSpell(@PathVariable("id") Long id) throws NotFoundException {
		Spell spell = this.spellService.getEntityById(id);
		return this.spellCardRenderer.convertSpellForRender(spell);
	}

	@GetMapping("/api/spellbook/{id}/render/info")
	@ApiOperation("Get spellbook render info by id")
	@ApiResponses({
		@ApiResponse(
			code = 200,
			message = "Success"
		),
		@ApiResponse(
			code = 404,
			message = "Spellbook with this id not found. Possible error key\n" +
				"* " + NotFoundException.ERROR_TEXT,
			response = String.class, responseContainer = "Map"
		)
	})
	public SpellbookRenderDTO renderSpellbook(@PathVariable("id") Long id) throws NotFoundException {
		Spellbook spellbook = this.spellbookService.getEntityById(id);
		return this.spellbookRenderer.convertSpellbookForRender(spellbook);
	}
}
